<?php
session_start();
$key = 'auth_form';


//connection to DB
$servername = "localhost";
$username = "root";
$dbpassword = "";
$dbname = "zenit";
try {
    $conn = new PDO("mysql:host=$servername; dbname=$dbname", $username, $dbpassword);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e){
    echo $sql2 . $e->getMessage();
}

//try this code for injection
//pas1'); INSERT INTO ZN_user( id, login, password) VALUES (NULL, 'log2', 'pas2


$login = htmlspecialchars($_POST['login'], ENT_QUOTES, 'UTF-8');
$pass = htmlspecialchars($_POST['password'], ENT_QUOTES, 'UTF-8');

if($_POST['submit']) {

    if (!$_POST['password'] && !$_POST['login']) {
        echo 'Fill in login and password';
    } elseif (!$_POST['login']) {
        echo "Fill in login";
    } elseif (!$_POST['password']) {
        echo 'Fill in password';
    } else {

        //I let users input only letters, dots, @, and numbers
        $pattern = '/^[0-9A-Za-zА-Яа-яі@\'\.]{1,}$/u';
        $true_login = preg_match($pattern, $_POST['login']);
        $true_pass = preg_match($pattern,$_POST['password']);

        if(!$true_login && !$true_pass){
            echo 'Use only letters, numbers, @, apostrophe, or period';

        } elseif (!$true_login){
            echo 'Change your login (Use only letters, numbers, @, apostrophe, or period)';

        } elseif (!$true_pass){
            echo 'Change your password (Use only letters, numbers, @, apostrophe, or period)';

        } else {

            //prevent sql injection
            $sql = $conn->prepare("INSERT INTO ZN_user( id, login, password) VALUES (NULL, ?, ?);");
            $sql->execute([$login, $pass]);

        }
    }

}
hash_equals('', '');

if(hash_equals($_SESSION['csrf:' . $key], $_POST['token'])){
    echo 'You are welcome!<br>';

?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>
    <body>

    <br>
    <a href="/">Exit</a>
    <br>

    <form action="" method="post">
        <input type="hidden" name="token" value="<?php echo $_POST['token'] ?>"> <br>
        <input class="lang_en" type="submit" name="lang_en" value="EN">
        <input class="lang_ua" type="submit" name="lang_ua" value="UA">
    </form>

    <table border="2px">
        <tr>
            <th>Author</th>
            <th>Book</th>
        </tr>

        <?php

            //task #2
            if($_POST['lang_en']){
                $sql2 = $conn->query("SELECT 
            `ZN_author`.`author_name_en`, 
            `ZN_books`.`book_name_en` 
            FROM 
                `ZN_author` 
                LEFT JOIN `ZN_books` ON `ZN_author`.`author_id` = `ZN_books`.`author_id` 
            WHERE 1");
                $sql2->execute();
                $data = $sql2->fetchAll();

                foreach($data as $row): ?>
                    <tr>
                        <td><?php echo $row['author_name_en']; ?></td>
                        <td><?php echo $row['book_name_en']; ?></td>
                    </tr>
                <?php endforeach;
            } else {

                $sql2 = $conn->query("SELECT 
            `ZN_author`.`author_name_ua`,
            `ZN_books`.`book_name`
            FROM 
                `ZN_author` 
                LEFT JOIN `ZN_books` ON `ZN_author`.`author_id` = `ZN_books`.`author_id`
            WHERE 1");
                $sql2->execute();
                $data = $sql2->fetchAll();
            foreach($data as $row): ?>
                <tr>
                    <td><?php echo $row['author_name_ua']; ?></td>
                    <td><?php echo $row['book_name']; ?></td>
                </tr>
            <?php endforeach;

            }
        ?>
    </table>

    </body>
    </html>

<?php
} else {
    echo 'CSRF error!';
}
?>
